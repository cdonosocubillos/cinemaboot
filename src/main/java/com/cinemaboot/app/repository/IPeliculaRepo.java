package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cinemaboot.app.entity.Pelicula;

public interface IPeliculaRepo extends JpaRepository<Pelicula, Integer>{

}
