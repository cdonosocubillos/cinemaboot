package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.VentaComida;

@Repository
public interface IVentaComidaRepo extends JpaRepository<VentaComida, Integer>{

	@Modifying
	@Query(value = "INSERT INTO ventas_comidas(id_venta, id_comida) VALUES (:idVenta, :idComida)", nativeQuery = true)
	Integer registrarCustom(@Param("idVenta") Integer idVenta, @Param("idComida") Integer idComida);
}
