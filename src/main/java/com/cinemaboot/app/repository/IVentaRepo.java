package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.Venta;

@Repository
public interface IVentaRepo extends JpaRepository<Venta, Integer>{

}
