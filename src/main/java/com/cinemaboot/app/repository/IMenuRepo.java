package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.Menu;

@Repository
public interface IMenuRepo extends JpaRepository<Menu, Integer>{

}
