package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.Rol;

@Repository
public interface IRolRepo extends JpaRepository<Rol, Integer>{

}
