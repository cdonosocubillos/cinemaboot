package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.DetalleVenta;

@Repository
public interface IDetalleVentaRepo extends JpaRepository<DetalleVenta, Integer>{

}
