package com.cinemaboot.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cinemaboot.app.entity.Genero;

@Repository
public interface IGeneroRepo extends JpaRepository<Genero, Integer>{

}
