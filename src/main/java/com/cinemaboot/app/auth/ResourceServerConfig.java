package com.cinemaboot.app.auth;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{

	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/api/clientes").permitAll()
		//SE PERMITIRA PARA TODOS LOS METODOS POR AHORA
		.antMatchers(HttpMethod.GET).permitAll()        //BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.DELETE).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.HEAD).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.OPTIONS).permitAll()	//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.PATCH).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.POST).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.PUT).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.antMatchers(HttpMethod.TRACE).permitAll()		//BORRAR PARA MANEJAR LOS PERMISOS
		.anyRequest().authenticated()
		.and().cors().configurationSource(corsConfigurationSource());
	}
	
	@Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200", "*")); //(*) nombre del server
        configuration.setAllowedMethods(Arrays.asList("GET","POST","PUT","DELETE","OPTIONS"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
	
	//Filtro de cors del metodo corsConfigurationSource()
	@Bean
	public FilterRegistrationBean<CorsFilter> corsfilter(){
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

}
