package com.cinemaboot.app.auth;

public class JwtConfig {
		
	public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\r\n" + 
			"MIIEowIBAAKCAQEA+76Y+CD0NtbXeHU7VIxKmYfKA6y8e/89tmXCu/rexWG94MxI\r\n" + 
			"E2O4dIiacSpQmYPFItUtg0FXqcjkfx36MLRD0/HD7N4iS5Bx6x7BqXV5FPy+sF28\r\n" + 
			"QCEWcjpsfPB/QQslZPFoiHy7TXkw95DMsEYIeT4OBCQx+YxMsRjdcpd7k3ly/8lx\r\n" + 
			"WTfwKMfYNuVA8Kq+Wb3O0AHPMm2rgdncwojdv06AcqxamBdm8XiU19fVbhNnRUy4\r\n" + 
			"pPW2hdzp7eRusZLrroZLXjVdI+7NqP+ugrLTtptytMrXbwLvHntgiPppBbSdfhNp\r\n" + 
			"Mt0U/uSaF4Lze5HdhrNhOG2GDMff+kM0nTg6JQIDAQABAoIBAQDP6XQQybaoa/t8\r\n" + 
			"4uVjSJJ8BmfTbSAmEIF5WMXh1qmRvs3BETYdvWgj5oYXHGmAwBFaWVHDa33pHZ+J\r\n" + 
			"VMrC2r6CFtRHlpYgbmWHV9l+CSU9LhiXNHM7S5hsMtBSx5VxtLDnk3FP+CQBMiw8\r\n" + 
			"6nwCwXRSHewZg4M6pKLaSLzr3SwUeMAquOpC6TtSJpwSILy/8+gkXNsDfWs+T2Eh\r\n" + 
			"FP10Phx+v2gJK93yPuYem2jdgxLooQJREAUSZSIbydsSdo6cDzEDUiVgstulDI/L\r\n" + 
			"LcncGcgaTtpWYgFTKr/aozYZHN/FLHd2P/BrDXMJVeJFJ3z4/6LHHGjuA74AGAZ6\r\n" + 
			"cdjLN1yBAoGBAP5u8Piq4JMuKQl2nPgGKw6iiHncjEhR8LvSKaxs+I0ePnpLVghU\r\n" + 
			"nppTO/10PXxMdZJZ/X2twLRjdHqzVGr+50oZANEPN0X5Hanou4JnndHGfGoiWv2e\r\n" + 
			"IFTNkgk5v6jKfnqK+i5IShuEKXcE9nb76Jfnkya5YlSuzUw7VZQmbx+1AoGBAP1L\r\n" + 
			"avleHk/5p+M6LChpA+jZZj1sf+jqXsNu1+pxOaAczTVhuwfHlmL34r+WUxakfOO7\r\n" + 
			"dlNP7QeAxAagBqPLp1dMAcCPQjctp19T33ZmiDGfV8Itv9TfYtbUr3vb7wFXFlS7\r\n" + 
			"KI+JFgjDGcB7SNT/pOcb7X9b15KhKM/ghsmnmtaxAoGAOzRxmkuY6mapYexZ5lT5\r\n" + 
			"G/SyMTki44k9rVbgH6FTzPKmsypooJOgPGT15qpvzEa2bRHHrMELwG4MIqksEaGz\r\n" + 
			"DEj5KaySvhOT1X+0cknOLYw0UzYfWji7aUBUpRTtBkfoWneQc8iahgCg7XMYLuO7\r\n" + 
			"Ye2fDjnBKddrKjv8U/cFkKkCgYAm/D+nyAO0XYGoqKB5S8iNQ0DIPXXmfIQ/YkUb\r\n" + 
			"UXgEG8q955/oz6Bm9S5SPKnnN0P9rTHGGuwplnkVZ/X9qe/EsO8oGySe54j2VCt1\r\n" + 
			"xsw5jwPr4CzBb+x/kla8IprDwwqt2eyhQwFAMf85sAACwKI57AN+8bzZVbK5ZB56\r\n" + 
			"1TAw0QKBgHs4BKQ02mIEajrfrkmlmtwXNrnlEBcDh9uqHVVEfPz33/Ci7qMyYbQ6\r\n" + 
			"aZO2D108uNGGJT4BeiZWEzRqA3fyov9ju95572JRmlovReN5fgRLJP/1kSjF/3Zn\r\n" + 
			"+XQREl5ruDTOtD9PSUMvJx+yQI1NeI9hzD0C9byzSRaUSRgflbJi\r\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\r\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA+76Y+CD0NtbXeHU7VIxK\r\n" + 
			"mYfKA6y8e/89tmXCu/rexWG94MxIE2O4dIiacSpQmYPFItUtg0FXqcjkfx36MLRD\r\n" + 
			"0/HD7N4iS5Bx6x7BqXV5FPy+sF28QCEWcjpsfPB/QQslZPFoiHy7TXkw95DMsEYI\r\n" + 
			"eT4OBCQx+YxMsRjdcpd7k3ly/8lxWTfwKMfYNuVA8Kq+Wb3O0AHPMm2rgdncwojd\r\n" + 
			"v06AcqxamBdm8XiU19fVbhNnRUy4pPW2hdzp7eRusZLrroZLXjVdI+7NqP+ugrLT\r\n" + 
			"tptytMrXbwLvHntgiPppBbSdfhNpMt0U/uSaF4Lze5HdhrNhOG2GDMff+kM0nTg6\r\n" + 
			"JQIDAQAB\r\n" + 
			"-----END PUBLIC KEY-----";
}
