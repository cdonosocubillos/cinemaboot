package com.cinemaboot.app.dto;

import java.util.List;

import org.springframework.hateoas.ResourceSupport;

import com.cinemaboot.app.entity.Comida;
import com.cinemaboot.app.entity.Venta;

public class VentaDTO extends ResourceSupport{

	public Venta venta;
	public List<Comida> lstComidas;
	
	public Venta getVenta() {
		return venta;
	}
	public void setVenta(Venta venta) {
		this.venta = venta;
	}
	public List<Comida> getLstComidas() {
		return lstComidas;
	}
	public void setLstComidas(List<Comida> lstComidas) {
		this.lstComidas = lstComidas;
	}
	
	
}
