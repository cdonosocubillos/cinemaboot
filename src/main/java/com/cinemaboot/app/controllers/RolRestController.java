package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cinemaboot.app.entity.Rol;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.RolServiceImpl;

@RestController
@RequestMapping(value = {"/api/roles"})
public class RolRestController {

	@Autowired
	private RolServiceImpl rolService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Rol>> listar(){
		 List<Rol> lista = rolService.listar();
		return new ResponseEntity<List<Rol>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Rol> registrar(@Valid @RequestBody Rol rol) {
		Rol obj = rolService.registrar(rol);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(rol.getIdRol()).toUri();
//		return ResponseEntity.created(location).build();
		return new ResponseEntity<Rol>(obj, HttpStatus.CREATED); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Rol> listarPorId(@PathVariable("id") Integer id){
		Rol obj = rolService.buscarPorId(id);
		if(obj.getIdRol() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Rol>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Rol> modificar(@Valid @RequestBody Rol rol) {
		Rol obj = rolService.modificar(rol);
		//URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(rol.getIdRol()).toUri();
		//return ResponseEntity.created(location).build();
		return new ResponseEntity<Rol>(obj, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Rol obj = rolService.buscarPorId(id);
		if(obj.getIdRol() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		rolService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
