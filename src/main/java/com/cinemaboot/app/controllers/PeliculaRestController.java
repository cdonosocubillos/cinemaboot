package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cinemaboot.app.entity.Genero;
import com.cinemaboot.app.entity.Pelicula;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.GeneroServiceImpl;
import com.cinemaboot.app.service.impl.PeliculaServiceImpl;

@RestController
@RequestMapping(value = {"/api/peliculas"})
public class PeliculaRestController {

	@Autowired
	private PeliculaServiceImpl peliculaService;
	
	@Autowired 
	private GeneroServiceImpl generoService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Pelicula>> listar(){
		 List<Pelicula> lista = peliculaService.listar();
		return new ResponseEntity<List<Pelicula>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Pelicula pelicula) {
		Genero genero = null;
		if (pelicula.getGenero() != null) {
			genero = generoService.buscarPorId(pelicula.getGenero().getIdGenero()); // buscamos un genero existente
		}else {
			throw new ModeloNotFoundException("ID GENERO NO ENCONTRADO " + pelicula.getGenero().getIdGenero());
		}
		pelicula.setGenero(genero); //seteamos el genero previamente registrado 
		Pelicula obj = peliculaService.registrar(pelicula);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pelicula.getIdPelicula()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Pelicula> listarPorId(@PathVariable("id") Integer id){
		Pelicula obj = peliculaService.buscarPorId(id);
		if(obj.getIdPelicula() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Pelicula>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Pelicula> modificar(@Valid @RequestBody Pelicula pelicula) {
		Genero genero = null;
		if (pelicula.getGenero() != null) {
			genero = generoService.buscarPorId(pelicula.getGenero().getIdGenero()); // buscamos un genero existente
		}else {
			throw new ModeloNotFoundException("ID GENERO NO ENCONTRADO " + pelicula.getGenero().getIdGenero());
		}
		pelicula.setGenero(genero);
		Pelicula obj = peliculaService.modificar(pelicula);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(pelicula.getIdPelicula()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Pelicula obj = peliculaService.buscarPorId(id);
		if(obj.getIdPelicula() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		peliculaService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
