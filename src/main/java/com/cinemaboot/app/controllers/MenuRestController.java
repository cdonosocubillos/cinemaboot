package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cinemaboot.app.entity.Menu;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.MenuServiceImpl;

@RestController
@RequestMapping(value = {"/api/menus"})
public class MenuRestController {

	@Autowired
	private MenuServiceImpl menuService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Menu>> listar(){
		 List<Menu> lista = menuService.listar();
		return new ResponseEntity<List<Menu>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Menu menu) {
		Menu obj = menuService.registrar(menu);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(menu.getIdMenu()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Menu> listarPorId(@PathVariable("id") Integer id){
		Menu obj = menuService.buscarPorId(id);
		if(obj.getIdMenu() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Menu>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Menu> modificar(@Valid @RequestBody Menu menu) {
		Menu obj = menuService.modificar(menu);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(menu.getIdMenu()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Menu obj = menuService.buscarPorId(id);
		if(obj.getIdMenu() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		menuService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
