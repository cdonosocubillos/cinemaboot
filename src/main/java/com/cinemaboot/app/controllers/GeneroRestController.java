package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cinemaboot.app.entity.Genero;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.GeneroServiceImpl;

@RestController
@RequestMapping(value = {"/api/generos"})
public class GeneroRestController {

	@Autowired
	private GeneroServiceImpl generoService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Genero>> listar(){
		 List<Genero> lista = generoService.listar();
		return new ResponseEntity<List<Genero>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Genero> registrar(@Valid @RequestBody Genero genero) {
		Genero obj = generoService.registrar(genero);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(genero.getIdGenero()).toUri();
//		return ResponseEntity.created(location).build();
		return new ResponseEntity<Genero>(obj, HttpStatus.CREATED); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Genero> listarPorId(@PathVariable("id") Integer id){
		Genero obj = generoService.buscarPorId(id);
		if(obj.getIdGenero() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Genero>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Genero> modificar(@Valid @RequestBody Genero genero) {
		Genero obj = generoService.modificar(genero);
		//URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(genero.getIdGenero()).toUri();
		//return ResponseEntity.created(location).build();
		return new ResponseEntity<Genero>(obj, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Genero obj = generoService.buscarPorId(id);
		if(obj.getIdGenero() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		generoService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
