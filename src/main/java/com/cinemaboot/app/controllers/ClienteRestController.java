package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cinemaboot.app.entity.Cliente;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.ClienteServiceImpl;

@RestController
@RequestMapping(value = {"/api/clientes"})
public class ClienteRestController {

	@Autowired
	private ClienteServiceImpl clienteService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Cliente>> listar(){
		 List<Cliente> lista = clienteService.listar();
		return new ResponseEntity<List<Cliente>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Cliente cliente) {
		Cliente obj = clienteService.registrar(cliente);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cliente.getIdCliente()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> listarPorId(@PathVariable("id") Integer id){
		Cliente obj = clienteService.buscarPorId(id);
		if(obj.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Cliente>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Cliente> modificar(@Valid @RequestBody Cliente cliente) {
		Cliente obj = clienteService.modificar(cliente);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(cliente.getIdCliente()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Cliente obj = clienteService.buscarPorId(id);
		if(obj.getIdCliente() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		clienteService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
