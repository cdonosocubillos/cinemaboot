package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cinemaboot.app.entity.Usuario;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.UsuarioServiceImpl;

@RestController
@RequestMapping(value = {"/api/usuarios"})
public class UsuarioRestController {

	@Autowired
	private UsuarioServiceImpl usuarioService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Usuario>> listar(){
		 List<Usuario> lista = usuarioService.listar();
		return new ResponseEntity<List<Usuario>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Usuario> registrar(@Valid @RequestBody Usuario usuario) {
		Usuario obj = usuarioService.registrar(usuario);
//		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuario.getIdUsuario()).toUri();
//		return ResponseEntity.created(location).build();
		return new ResponseEntity<Usuario>(obj, HttpStatus.CREATED); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> listarPorId(@PathVariable("id") Integer id){
		Usuario obj = usuarioService.buscarPorId(id);
		if(obj.getIdUsuario() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Usuario>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Usuario> modificar(@Valid @RequestBody Usuario usuario) {
		Usuario obj = usuarioService.modificar(usuario);
		//URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuario.getIdUsuario()).toUri();
		//return ResponseEntity.created(location).build();
		return new ResponseEntity<Usuario>(obj, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Usuario obj = usuarioService.buscarPorId(id);
		if(obj.getIdUsuario() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		usuarioService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
