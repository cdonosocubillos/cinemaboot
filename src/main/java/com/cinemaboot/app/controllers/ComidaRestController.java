package com.cinemaboot.app.controllers;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.cinemaboot.app.entity.Comida;
import com.cinemaboot.app.exception.ModeloNotFoundException;
import com.cinemaboot.app.service.impl.ComidaServiceImpl;

@RestController
@RequestMapping(value = {"/api/comidas"})
public class ComidaRestController {

	@Autowired
	private ComidaServiceImpl comidaService;
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping
	public ResponseEntity<List<Comida>> listar(){
		 List<Comida> lista = comidaService.listar();
		return new ResponseEntity<List<Comida>>(lista, HttpStatus.OK);
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PostMapping
	public ResponseEntity<Object> registrar(@Valid @RequestBody Comida comida) {
		Comida obj = comidaService.registrar(comida);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(comida.getIdComida()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@GetMapping("/{id}")
	public ResponseEntity<Comida> listarPorId(@PathVariable("id") Integer id){
		Comida obj = comidaService.buscarPorId(id);
		if(obj.getIdComida() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<Comida>(obj, HttpStatus.OK); 
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@PutMapping
	public ResponseEntity<Comida> modificar(@Valid @RequestBody Comida comida) {
		Comida obj = comidaService.modificar(comida);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(comida.getIdComida()).toUri();
		return ResponseEntity.created(location).build();
	}
	
//	@Secured({"ROLE_ADMIN","ROLE_USER"})
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> eliminar(@PathVariable("id") Integer id){
		Comida obj = comidaService.buscarPorId(id);
		if(obj.getIdComida() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		comidaService.eliminar(id);
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}
}
