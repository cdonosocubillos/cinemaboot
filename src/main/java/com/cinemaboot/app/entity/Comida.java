package com.cinemaboot.app.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "comidas")
public class Comida implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idComida;
	
	@ApiModelProperty(notes = "Nombre debe tener máximo 80 carácteres", allowEmptyValue = false)
	@Size(max = 80, message = "Nombre debe tener máximo 80 carácteres")
	@NotNull(message = "No puede ser nulo")
	@Column(name = "nombre", nullable = false, length = 80)
	private String nombre;
	
	@ApiModelProperty(notes = "Precio no puede ser nulo", allowEmptyValue = false)
	@NotNull(message = "Precio no puede ser nulo")
	@Column(name = "precio", nullable = false)
	private Double precio;
	
	@ApiModelProperty(notes = "foto del producto", allowEmptyValue = false)
	@Column(name = "foto", nullable = true)
	private Byte[] foto;
	
	public Integer getIdComida() {
		return idComida;
	}

	public void setIdComida(Integer idComida) {
		this.idComida = idComida;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Byte[] getFoto() {
		return foto;
	}

	public void setFoto(Byte[] foto) {
		this.foto = foto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(foto);
		result = prime * result + ((idComida == null) ? 0 : idComida.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((precio == null) ? 0 : precio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comida other = (Comida) obj;
		if (!Arrays.equals(foto, other.foto))
			return false;
		if (idComida == null) {
			if (other.idComida != null)
				return false;
		} else if (!idComida.equals(other.idComida))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (precio == null) {
			if (other.precio != null)
				return false;
		} else if (!precio.equals(other.precio))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;

}
