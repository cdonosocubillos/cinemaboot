package com.cinemaboot.app.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "peliculas")
public class Pelicula implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPelicula;
	
	@ApiModelProperty(notes = "Nombre debe tener máximo 255 carácteres", allowEmptyValue = false)
	@Size(max = 255, message = "Nombre debe tener máximo 255 carácteres")
	@NotNull(message = "No puede ser nulo")
	@Column(name = "nombre", nullable = false, length = 255)
	private String nombre;
	
	@ApiModelProperty(notes = "Resena debe tener máximo 255 carácteres", allowEmptyValue = false)
	@Size(max = 255, message = "Resena debe tener máximo 255 carácteres")
	@NotNull(message = "No puede ser nulo")
	@Column(name = "resena", nullable = false, length = 255)
	private String resena;
	
	@ApiModelProperty(notes = "Duracion debe tener máximo 3 carácteres", allowEmptyValue = false)
	@Range(min=0, max=999)
	@NotNull(message = "No puede ser nulo")
	@Column(name = "duracion", nullable = false, length = 3)
	private Short duracion;
	
	@ApiModelProperty(notes = "Fecha de publicacion", allowEmptyValue = false)
	@Column(name = "fecha_publicacion")
	private LocalDate fechaPublicacion;
	
	@ApiModelProperty(notes = "Nombre debe tener máximo 255 carácteres", allowEmptyValue = false)
	@Size(max = 255, message = "Nombre debe tener máximo 255 carácteres")
	@NotNull(message = "No puede ser nulo")
	@Column(name = "url_portada", nullable = false, length = 255)
	private String urlPortada;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "id_genero", nullable = false, foreignKey = @ForeignKey(name = "fk_pelicula_genero"))
	private Genero genero;
	
	@OneToMany(mappedBy = "pelicula", cascade = { CascadeType.ALL })
	@JsonIgnore
	private List<Venta> ventas;

	public Integer getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Integer idPelicula) {
		this.idPelicula = idPelicula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getResena() {
		return resena;
	}

	public void setResena(String resena) {
		this.resena = resena;
	}

	public Short getDuracion() {
		return duracion;
	}

	public void setDuracion(Short duracion) {
		this.duracion = duracion;
	}

	public LocalDate getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(LocalDate fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public String getUrlPortada() {
		return urlPortada;
	}

	public void setUrlPortada(String urlPortada) {
		this.urlPortada = urlPortada;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public List<Venta> getVentas() {
		return ventas;
	}

	public void setVentas(List<Venta> ventas) {
		this.ventas = ventas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((duracion == null) ? 0 : duracion.hashCode());
		result = prime * result + ((fechaPublicacion == null) ? 0 : fechaPublicacion.hashCode());
		result = prime * result + ((genero == null) ? 0 : genero.hashCode());
		result = prime * result + ((idPelicula == null) ? 0 : idPelicula.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((resena == null) ? 0 : resena.hashCode());
		result = prime * result + ((urlPortada == null) ? 0 : urlPortada.hashCode());
		result = prime * result + ((ventas == null) ? 0 : ventas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pelicula other = (Pelicula) obj;
		if (duracion == null) {
			if (other.duracion != null)
				return false;
		} else if (!duracion.equals(other.duracion))
			return false;
		if (fechaPublicacion == null) {
			if (other.fechaPublicacion != null)
				return false;
		} else if (!fechaPublicacion.equals(other.fechaPublicacion))
			return false;
		if (genero == null) {
			if (other.genero != null)
				return false;
		} else if (!genero.equals(other.genero))
			return false;
		if (idPelicula == null) {
			if (other.idPelicula != null)
				return false;
		} else if (!idPelicula.equals(other.idPelicula))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (resena == null) {
			if (other.resena != null)
				return false;
		} else if (!resena.equals(other.resena))
			return false;
		if (urlPortada == null) {
			if (other.urlPortada != null)
				return false;
		} else if (!urlPortada.equals(other.urlPortada))
			return false;
		if (ventas == null) {
			if (other.ventas != null)
				return false;
		} else if (!ventas.equals(other.ventas))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;
}
