package com.cinemaboot.app.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table(name = "ventas_comidas")
@IdClass(VentaComidaPK.class)
public class VentaComida {

	@Id
	private Venta idVenta;
	
	@Id
	private Comida idComida;
	
	public Venta getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(Venta idVenta) {
		this.idVenta = idVenta;
	}
	public Comida getIdComida() {
		return idComida;
	}
	public void setIdComida(Comida idComida) {
		this.idComida = idComida;
	}
	
}
