package com.cinemaboot.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "configs")
public class Config implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idConfig;
	
	@ApiModelProperty(notes = "Parametro debe tener máximo 5 carácteres")
	@Size(max = 5, message = "Parametro debe tener máximo 5 carácteres")
	@Column(name = "parametro", length = 5)
	private String parametro;
	
	@ApiModelProperty(notes = "Valor debe tener máximo 25 carácteres")
	@Size(max = 25, message = "Valor debe tener máximo 25 carácteres")
	@Column(name = "valor", length = 25)
	private String valor;
	
	public Integer getIdConfig() {
		return idConfig;
	}

	public void setIdConfig(Integer idConfig) {
		this.idConfig = idConfig;
	}

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idConfig == null) ? 0 : idConfig.hashCode());
		result = prime * result + ((parametro == null) ? 0 : parametro.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Config other = (Config) obj;
		if (idConfig == null) {
			if (other.idConfig != null)
				return false;
		} else if (!idConfig.equals(other.idConfig))
			return false;
		if (parametro == null) {
			if (other.parametro != null)
				return false;
		} else if (!parametro.equals(other.parametro))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;

}
