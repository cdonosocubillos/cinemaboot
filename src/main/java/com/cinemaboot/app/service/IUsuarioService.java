package com.cinemaboot.app.service;

import com.cinemaboot.app.entity.Usuario;

public interface IUsuarioService extends ICRUD<Usuario>{

	public Usuario findByUserName(String userName);
}
