package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Usuario;
import com.cinemaboot.app.repository.IUsuarioRepo;
import com.cinemaboot.app.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService, UserDetailsService{

	private Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	private IUsuarioRepo repo;
	
	@Override
	@Transactional
	public Usuario registrar(Usuario obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional
	public Usuario modificar(Usuario obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> listar() {
		return repo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario buscarPorId(Integer id) {
		Optional<Usuario> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Usuario();
	}

	@Override
	@Transactional
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

	@Override
	public Usuario findByUserName(String userName) {
		return repo.findByUserName(userName);
	}

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		Usuario usuario = repo.findByUserName(userName);
		
		if(usuario == null) {
			logger.error("Error en el login: no existe el usuario '"+userName+"' en el sistema!");
			throw new UsernameNotFoundException("Error en el login: no existe el usuario '"+userName+"' en el sistema!");
		}
		List<GrantedAuthority> authorities = usuario.getRoles()
													.stream()
													.map(role -> new SimpleGrantedAuthority(role.getNombre()))
													.peek(authority -> logger.info("Role: " + authority.getAuthority()))
													.collect(Collectors.toList());


		return new User(usuario.getUserName(), usuario.getClave(), usuario.getEnabled(), true, true, true, authorities);
	}

}
