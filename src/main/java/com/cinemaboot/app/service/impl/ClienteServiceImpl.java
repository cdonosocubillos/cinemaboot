package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Cliente;
import com.cinemaboot.app.repository.IClienteRepo;
import com.cinemaboot.app.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	private IClienteRepo repo;
	
	@Transactional
	@Override
	public Cliente registrar(Cliente obj) {
		return repo.save(obj);
	}

	@Transactional
	@Override
	public Cliente modificar(Cliente obj) {
		return repo.save(obj);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Cliente> listar() {
		return repo.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Cliente buscarPorId(Integer id) {
		Optional<Cliente> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Cliente();
	}

	@Transactional
	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
