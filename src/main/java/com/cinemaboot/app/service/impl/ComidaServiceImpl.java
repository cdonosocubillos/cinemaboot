package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Comida;
import com.cinemaboot.app.repository.IComidaRepo;
import com.cinemaboot.app.service.IComidaService;

@Service
public class ComidaServiceImpl implements IComidaService{
	
	@Autowired
	private IComidaRepo repo;

	@Transactional
	@Override
	public Comida registrar(Comida obj) {
		return repo.save(obj);
	}

	@Transactional
	@Override 
	public Comida modificar(Comida obj) {
		return repo.save(obj);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Comida> listar() {
		return repo.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Comida buscarPorId(Integer id) {
		Optional<Comida> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Comida();
	}

	@Transactional
	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
