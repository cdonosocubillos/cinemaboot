package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Rol;
import com.cinemaboot.app.repository.IRolRepo;
import com.cinemaboot.app.service.IRolService;

@Repository
public class RolServiceImpl implements IRolService{

	@Autowired
	private IRolRepo repo;
	
	@Override
	@Transactional
	public Rol registrar(Rol obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional
	public Rol modificar(Rol obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Rol> listar() {
		return repo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Rol buscarPorId(Integer id) {
		Optional<Rol> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Rol();
	}

	@Override
	@Transactional
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
