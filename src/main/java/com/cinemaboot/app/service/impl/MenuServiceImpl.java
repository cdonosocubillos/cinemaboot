package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Menu;
import com.cinemaboot.app.repository.IMenuRepo;
import com.cinemaboot.app.service.IMenuService;

@Service
public class MenuServiceImpl implements IMenuService{

	@Autowired
	private IMenuRepo repo;
	
	@Override
	@Transactional
	public Menu registrar(Menu obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional
	public Menu modificar(Menu obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Menu> listar() {
		return repo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Menu buscarPorId(Integer id) {
		Optional<Menu> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Menu();
	}

	@Override
	@Transactional
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
