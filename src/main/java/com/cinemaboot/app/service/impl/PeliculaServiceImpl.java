package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Pelicula;
import com.cinemaboot.app.repository.IPeliculaRepo;
import com.cinemaboot.app.service.IPeliculaService;

@Service
public class PeliculaServiceImpl implements IPeliculaService{

	@Autowired
	private IPeliculaRepo repo;
	
	@Transactional
	@Override
	public Pelicula registrar(Pelicula obj) {
		return repo.save(obj);
	}

	@Transactional
	@Override
	public Pelicula modificar(Pelicula obj) {
		return repo.save(obj);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Pelicula> listar() {
		return repo.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Pelicula buscarPorId(Integer id) {
		Optional<Pelicula> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Pelicula();
	}

	@Transactional
	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
