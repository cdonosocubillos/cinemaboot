package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.dto.VentaDTO;
import com.cinemaboot.app.entity.Venta;
import com.cinemaboot.app.repository.IVentaComidaRepo;
import com.cinemaboot.app.repository.IVentaRepo;
import com.cinemaboot.app.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaRepo ventaRepo;
	
	@Autowired
	private IVentaComidaRepo ventaComidaRepo;
	
	@Transactional
	@Override
	public Venta registrar(Venta obj) {
		obj.getDetalleVenta().forEach(det -> {
			det.setVenta(obj);
		});
		return ventaRepo.save(obj);
	}

	@Transactional
	@Override
	public Venta modificar(Venta obj) {
		obj.getDetalleVenta().forEach(det -> {
			det.setVenta(obj);
		});
		return ventaRepo.save(obj);
	}

	@Transactional(readOnly = true)
	@Override
	public List<Venta> listar() {
		return ventaRepo.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Venta buscarPorId(Integer id) {
		Optional<Venta> op = ventaRepo.findById(id);
		return op.isPresent() ? op.get() : new Venta();
	}

	@Transactional
	@Override
	public boolean eliminar(Integer id) {
		ventaRepo.deleteById(id);
		return true;
	}

	@Transactional
	@Override
	public Venta registrarTransaccional(VentaDTO dto) {
		registrar(dto.getVenta());
		dto.getLstComidas()
		.forEach(com -> ventaComidaRepo.registrarCustom(dto.getVenta().getIdVenta(), com.getIdComida())); // relacion venta_comida
		return dto.getVenta();
	}

}
