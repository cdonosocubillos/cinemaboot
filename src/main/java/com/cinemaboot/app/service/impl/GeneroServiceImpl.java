package com.cinemaboot.app.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cinemaboot.app.entity.Genero;
import com.cinemaboot.app.repository.IGeneroRepo;
import com.cinemaboot.app.service.IGeneroService;

@Service
public class GeneroServiceImpl implements IGeneroService{

	@Autowired
	private IGeneroRepo repo;
	
	@Override
	@Transactional
	public Genero registrar(Genero obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional
	public Genero modificar(Genero obj) {
		return repo.save(obj);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Genero> listar() {
		return repo.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Genero buscarPorId(Integer id) {
		Optional<Genero> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Genero();
	}

	@Override
	@Transactional
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

}
