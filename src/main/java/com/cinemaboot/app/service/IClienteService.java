package com.cinemaboot.app.service;

import org.springframework.stereotype.Service;

import com.cinemaboot.app.entity.Cliente;

@Service
public interface IClienteService extends ICRUD<Cliente>{

}
