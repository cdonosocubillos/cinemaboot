package com.cinemaboot.app.service;

import com.cinemaboot.app.dto.VentaDTO;
import com.cinemaboot.app.entity.Venta;

public interface IVentaService extends ICRUD<Venta>{

	Venta registrarTransaccional(VentaDTO dto); 
}
