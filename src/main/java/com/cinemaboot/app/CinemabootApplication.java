package com.cinemaboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemabootApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinemabootApplication.class, args);
	}

}
